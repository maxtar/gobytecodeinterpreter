package regvm

import "fmt"

// RegistryVM structs presents registry virtual machine
type RegistryVM struct {
	ip int
	// Register array
	reg [16]int64
	// A single register containing the result
	result int64
}

const (
	// OpDone command stop execution
	OpDone = iota
	// OpLoadi command load an immediate value into r0
	OpLoadi
	// OpAdd comman add values in r0,r1 registers and put them into r2
	OpAdd
	// OpSub command subtract values in r0,r1 registers and put them into r2
	OpSub
	// OpDiv command Divide values in r0,r1 registers and put them into r2
	OpDiv
	// OpMul command multiply values in r0,r1 registers and put them into r2
	OpMul
	// OpMovRes command move a value from r0 register into the result register
	OpMovRes
)

const (
	// ResultSuccess success interpreter parsing result
	ResultSuccess = iota
	// ResultErrorDivisionByZero in approproate case
	ResultErrorDivisionByZero
	// ResultErrUnkOpCode incorrect operation
	ResultErrUnkOpCode
)

// Reset reset current VM state
func (vm *RegistryVM) Reset() {
	fmt.Println("Reset VM state")
	vm.ip = 0
	vm.reg = [16]int64{}
	vm.result = 0
}

// Decode method parse VM command and initiate regustries
func decode(instruction uint16, op, reg0, reg1, reg2, imm *byte) {
	*op = byte((instruction & 0xF000) >> 12)
	*reg0 = byte((instruction & 0x0F00) >> 8)
	*reg1 = byte((instruction & 0x00F0) >> 4)
	*reg2 = byte((instruction & 0x000F))
	*imm = byte((instruction & 0x00FF))
}

// Interpret Execute bytecode program
// todo Add tests for this VM
func (vm *RegistryVM) Interpret(bytecode []uint16) int {
	vm.Reset()
	fmt.Println("Start interpreting")

	var op, r0, r1, r2, immediate byte

	for {
		// fetch the instruction
		instruction := bytecode[vm.ip]
		vm.ip++
		// decode it
		decode(instruction, &op, &r0, &r1, &r2, &immediate)
		// dispatch
		switch op {
		case OpLoadi:
			vm.reg[r0] = int64(immediate)
		case OpAdd:
			vm.reg[r2] = vm.reg[r0] + vm.reg[r1]
			break
		case OpSub:
			vm.reg[r2] = vm.reg[r0] - vm.reg[r1]
			break
		case OpDiv:
			// Don't forget to handle the div by zero error
			if vm.reg[r1] == 0 {
				return ResultErrorDivisionByZero
			}
			vm.reg[r2] = vm.reg[r0] / vm.reg[r1]
			break
		case OpMul:
			vm.reg[r2] = vm.reg[r0] * vm.reg[r1]
			break
		case OpMovRes:
			vm.result = vm.reg[r0]
			break
		case OpDone:
			return ResultSuccess
		default:
			return ResultErrUnkOpCode
		}
	}
}
