package stackvm

import (
	"testing"
)

type testCase struct {
	program    []byte
	programRes int
	result     int64
}

var (
	testCases = []testCase{
		testCase{program: []byte{OpPushi, 1, OpPushi, 2, OpAdd, OpPopRes, OpDone}, programRes: ResultSuccess, result: 3},
		testCase{program: []byte{OpPushi, 3, OpPushi, 2, OpMul, OpPopRes, OpDone}, programRes: ResultSuccess, result: 6},
		testCase{program: []byte{OpPushi, 3, OpPushi, 2, OpMul, OpDone}, programRes: ResultSuccess, result: 0},
		testCase{program: []byte{OpPushi, 3, OpPushi, 2, OpMul, OpPushi, 7, OpMul, OpPopRes, OpDone}, programRes: ResultSuccess, result: 42},
		testCase{program: []byte{OpPushi, 4, OpPushi, 2, OpDiv, OpPushi, 0, OpDiv, OpPopRes, OpDone}, programRes: ResultErrorDivisionByZero, result: 0},
		testCase{program: []byte{OpPushi, 3, OpPushi, 2, OpPushi, 2, OpSub, OpDiv, OpPopRes, OpDone}, programRes: ResultErrorDivisionByZero, result: 0},
	}
)

func TestVM(t *testing.T) {
	for i, tc := range testCases {

		vm := StackVM{}
		if programRes := vm.Interpret(tc.program); programRes != tc.programRes {
			t.Errorf("%d: Program must to be %d, but was %d\n", i, tc.programRes, programRes)
		}
		if acc := vm.result; acc != tc.result {
			t.Errorf("%d: Result must to be equal %d, but was %d\n", i, tc.result, acc)
		}
	}
}
