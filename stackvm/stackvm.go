package stackvm

import (
	"fmt"
)

// StackVM structs presents stack virtual machine
type StackVM struct {
	ip int

	// Fixed-size stack
	stack    [256]int64
	stackTop int64

	// A single register containing the result
	result int64
}

const (
	// OpDone command stop execution
	OpDone = iota
	// OpPushi command push the immediate argument onto the stack
	OpPushi
	// OpAdd command pop 2 values from the stack, add and push the result onto the stack
	OpAdd
	// OpSub command pop 2 values from the stack, subtract and push the result onto the stack
	OpSub
	// OpDiv command pop 2 values from the stack, divide and push the result onto the stack
	OpDiv
	// OpMul command pop 2 values from the stack, multiply and push the result onto the stack
	OpMul
	// OpPopRes command pop the top of the stack and set it as execution result
	OpPopRes
)

const (
	// ResultSuccess success interpreter parsing result
	ResultSuccess = iota
	// ResultErrorDivisionByZero in approproate case
	ResultErrorDivisionByZero
	// ResultErrUnkOpCode incorrect operation
	ResultErrUnkOpCode
)

// Reset reset current VM state
func (vm *StackVM) Reset() {
	fmt.Println("Reset VM state")
	vm.ip = 0
	vm.stack = [256]int64{}
	vm.stackTop = 0
	vm.result = 0
}

// StackPush puth value to stack
func (vm *StackVM) StackPush(value int64) {
	vm.stack[vm.stackTop] = value
	vm.stackTop++
}

// StackPop return last value from stack
func (vm *StackVM) StackPop() int64 {
	vm.stackTop--
	return vm.stack[vm.stackTop]
}

// Interpret Execute bytecode program
func (vm *StackVM) Interpret(bytecode []byte) int {
	vm.Reset()
	fmt.Println("Start interpreting")

	for {
		instruction := bytecode[vm.ip]
		vm.ip++
		switch instruction {
		case OpPushi:
			// get the argument, push it onto stack
			arg := bytecode[vm.ip]
			vm.ip++
			vm.StackPush(int64(arg))
			break
		case OpAdd:
			// Pop 2 values, add 'em, push the result back to the stack
			argRight := vm.StackPop()
			argLeft := vm.StackPop()
			vm.StackPush(argLeft + argRight)
			break
		case OpSub:
			// Pop 2 values, subtract 'em, push the result back to the stack
			argRight := vm.StackPop()
			argLeft := vm.StackPop()
			vm.StackPush(argLeft - argRight)
			break
		case OpDiv:
			// Pop 2 values, divide 'em, push the result back to the stack
			argRight := vm.StackPop()
			// Don't forget to handle the div by zero error
			if argRight == 0 {
				return ResultErrorDivisionByZero
			}
			argLeft := vm.StackPop()
			vm.StackPush(argLeft / argRight)
			break
		case OpMul:
			// Pop 2 values, multiply 'em, push the result back to the stack
			argRight := vm.StackPop()
			argLeft := vm.StackPop()
			vm.StackPush(argLeft * argRight)
			break
		case OpPopRes:
			// Pop the top of the stack, set it as a result value
			vm.result = vm.StackPop()
			break
		case OpDone:
			return ResultSuccess
		default:
			return ResultErrUnkOpCode
		}
	}
}
