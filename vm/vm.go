package vm

import (
	"fmt"
)

// VM structs presents virtual machine
type VM struct {
	ip          int
	accumulator int64
}

const (
	// OpDone command stop execution
	OpDone byte = iota
	// OpInc command increment the register
	OpInc
	// OpDec command decrement the register
	OpDec
	// OpAddi command add the immediate argument to the register
	OpAddi
	// OpSubi command subtract the immediate argument from the register
	OpSubi
)

const (
	// ResultSuccess success interpreter parsing result
	ResultSuccess = iota
	// ResultErrUnkOpCode incorrect operation
	ResultErrUnkOpCode
)

// Reset reset current VM state
func (vm *VM) Reset() {
	fmt.Println("Reset VM state")
	vm.ip = 0
	vm.accumulator = 0
}

// Interpret Execute bytecode program
func (vm *VM) Interpret(bytecode []byte) int {
	vm.Reset()

	fmt.Println("Start interpreting")

	for {
		instruction := bytecode[vm.ip]
		vm.ip++
		switch instruction {
		case OpInc:
			vm.accumulator++
			break
		case OpDec:
			vm.accumulator--
			break
		case OpAddi:
			arg := bytecode[vm.ip]
			vm.ip++
			vm.accumulator += int64(arg)
			break
		case OpSubi:
			arg := bytecode[vm.ip]
			vm.ip++
			vm.accumulator -= int64(arg)
		case OpDone:
			return ResultSuccess
		default:
			return ResultErrUnkOpCode
		}
	}

}
