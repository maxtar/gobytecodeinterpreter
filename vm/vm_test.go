package vm

import (
	"testing"
)

type testCase struct {
	program    []byte
	programRes int
	acc        int64
}

var (
	testCases = []testCase{
		testCase{program: []byte{OpInc, OpDone}, programRes: ResultSuccess, acc: 1},
		testCase{program: []byte{OpInc, OpInc, OpDone}, programRes: ResultSuccess, acc: 2},
		testCase{program: []byte{OpInc, OpInc, OpDec, OpDone}, programRes: ResultSuccess, acc: 1},
		testCase{program: []byte{OpInc, OpDec, OpDone}, programRes: ResultSuccess, acc: 0},
		testCase{program: []byte{OpInc, OpAddi, 4, OpDone}, programRes: ResultSuccess, acc: 5},
		testCase{program: []byte{OpAddi, 4, OpDone}, programRes: ResultSuccess, acc: 4},
		testCase{program: []byte{OpDec, OpDone}, programRes: ResultSuccess, acc: -1},
		testCase{program: []byte{OpDec, 5, OpDone}, programRes: ResultErrUnkOpCode, acc: -1},
		testCase{program: []byte{5, OpDone}, programRes: ResultErrUnkOpCode, acc: 0},
		testCase{program: []byte{OpAddi, 3, OpDec, OpDone}, programRes: ResultSuccess, acc: 2},
		testCase{program: []byte{OpAddi, 10, OpSubi, 2, OpDone}, programRes: ResultSuccess, acc: 8},
		testCase{program: []byte{OpAddi, 10, OpSubi, 2, OpDec, OpDone}, programRes: ResultSuccess, acc: 7},
	}
)

func TestVM(t *testing.T) {
	for i, tc := range testCases {

		vm := VM{}
		if programRes := vm.Interpret(tc.program); programRes != tc.programRes {
			t.Errorf("%d: Program must to be %d, but was %d\n", i, tc.programRes, programRes)
		}
		if acc := vm.accumulator; acc != tc.acc {
			t.Errorf("%d: Accumulator must to be equal %d, but was %d\n", i, tc.acc, acc)
		}
	}
}
